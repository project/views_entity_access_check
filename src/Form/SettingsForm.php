<?php

namespace Drupal\views_entity_access_check\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Views;

/**
 * Configure Views entity_access check settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'views_entity_access_check_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['views_entity_access_check.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $viewsSelectionList = $this->getAllViewsSelectionList();
    $form['views'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->t('Enable on the following Views:'),
      '#options' => $viewsSelectionList,
      '#default_value' => $this->config('views_entity_access_check.settings')->get('views'),
      '#description' => $this->t('Select the views where to run the <code>$value->_entity->access("view")</code> on pre-render, to sort out entities, where the user has no "view" access.<br /><strong>This should only be used on views, where needed, as it impacts performance and caching.</strong>'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $handleViews = array_keys($form_state->getValue('views'));
    $this->config('views_entity_access_check.settings')
      ->set('views', $handleViews)
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Helper function to return a key-value list of all selectable views.
   *
   * @return array
   *   The list of allowed tags.
   *
   * @internal
   */
  protected function getAllViewsSelectionList() {
    $views = Views::getAllViews();
    $viewsSelectionList = [];
    if (!empty($views)) {
      foreach ($views as $view) {
        $viewId = $view->id();
        $viewsSelectionList[$viewId] = $view->label() . ' (' . $viewId . ')';
      }
    }
    return $viewsSelectionList;
  }

}
